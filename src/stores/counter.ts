// import { defineStore } from "pinia";

// export const useCounterStore = defineStore('counter', {
// 	state: () => {
// 		return { count: 0 }
// 	},
// 	getters: {
// 		countValue: (state) => state.count
// 	},
// 	actions: {
// 		increment() {
// 			this.count++
// 		},
// 		decrement() {
// 			this.count--
// 		}
// 	}
// });

//Composition API
import { ref, computed } from "vue";
import { defineStore } from "pinia";

export const useCounterStore = defineStore('counter', () => {
	// state
	const count = ref(0)

	// getters
	const countValue = computed(() =>
		count.value
	)

	// actions
	const increment = () => {
		count.value++
	}

	const decrement = () => {
		count.value--
	}

	return {
		count,
		countValue,
		increment,
		decrement
	}
});