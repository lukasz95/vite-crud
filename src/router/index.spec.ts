import { mount } from '@vue/test-utils';
import { createRouter, createWebHistory } from 'vue-router'
import App from '../App.vue'

const routes = [
  {
    path: '/',
		name: 'HomePage',
    component: () => import(/* webpackChunkName: "homePage" */ '@/views/HomePage/HomePage.vue')
  },
	{
    path: '/about',
		name: 'AboutPage',
    component: () => import(/* webpackChunkName: "aboutPage" */ '@/views/AboutPage/AboutPage.vue')
  },
  {
    path: '/counter',
		name: 'CounterPage',
    component: () => import(/* webpackChunkName: "counterPage" */ '@/views/CounterPage/CounterPage.vue')
  }
];

const router = createRouter({
	history: createWebHistory(),
	routes
})

test('routing', async () => {
  router.push('/about')

  // After this line, router is ready
  await router.isReady()

  const wrapper = mount(App, {
    global: {
      plugins: [router]
    }
  })
  const text = wrapper.find('h1').text();
  expect(text).toBe('About page');
});