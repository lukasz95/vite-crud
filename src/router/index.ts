import { createRouter, createWebHistory } from 'vue-router'
import HomePage from '@/views/HomePage/HomePage.vue'

const routes = [
  {
    path: '/',
		name: 'HomePage',
    component: HomePage,
  },
  {
    path: '/404',
    name: '_404Page',
    component: () => import(/* webpackChunkName: "_404Page" */ '@/views/_404Page/_404Page.vue')
  },
  {
    path: '/:catchAll(.*)',
    redirect: '/404'
  },
	{
    path: '/about',
		name: 'AboutPage',
    component: () => import(/* webpackChunkName: "aboutPage" */ '@/views/AboutPage/AboutPage.vue')
  },
  {
    path: '/counter',
		name: 'CounterPage',
    component: () => import(/* webpackChunkName: "counterPage" */ '@/views/CounterPage/CounterPage.vue')
  }
];

const router = createRouter({
	scrollBehavior: () => ({ left: 0, top: 0 }),
	history: createWebHistory(),
	routes
})

export default router