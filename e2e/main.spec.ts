/// <reference types="cypress" />
describe('Home page', () => {
  it('should display header text', () => {
    cy.visit('/')
    cy.contains('h1', 'Home page')
  })
})

describe('Redirect test', () => {
  it('it should redirect to Home page', () => {
    cy.visit('/404');

    cy.contains('Page not found');
    cy.contains('a', 'Home page');

    cy.get('a.btn-success').click();
    cy.url().should('be.equal', 'http://localhost:3000/')
  });
});