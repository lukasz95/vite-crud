import { createApp } from 'vue'
import { createPinia } from 'pinia'
import router from './router'
import App from './App.vue'

import './styles/main.scss'

const store = createPinia();
const app = createApp(App);

app.use(router).use(store).mount('#app');
