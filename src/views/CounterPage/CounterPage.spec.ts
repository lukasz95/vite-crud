import { mount } from '@vue/test-utils'
import { createTestingPinia } from '@pinia/testing'
import CounterPage from './CounterPage.vue'

describe('CounterPage.vue', () => {
  it('Renders a Counter Page', () => {
    const wrapper = mount(CounterPage, { global: {
      plugins: [createTestingPinia()],
    }})
    expect(wrapper.text()).toMatch('Counter page')
  })
})
