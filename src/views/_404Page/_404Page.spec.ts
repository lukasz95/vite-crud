import { mount } from '@vue/test-utils'

import _404Page from './_404Page.vue'

describe('_404Page.vue', () => {
  it('Renders a 404 Page', () => {
    const wrapper = mount(_404Page)
    expect(wrapper.text()).toMatch('Page not found')
  
    expect(wrapper.html()).toContain('<router-link to="/" class="btn btn-success">Home page</router-link>');
  })
})