import { mount } from '@vue/test-utils'

import HomePage from './HomePage.vue'

describe('HomePage.vue', () => {
  it('Renders a Home Page', () => {
    const wrapper = mount(HomePage)
    expect(wrapper.text()).toMatch('Home page')
  })
})