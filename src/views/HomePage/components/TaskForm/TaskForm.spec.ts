import { mount } from '@vue/test-utils'
import { createTestingPinia } from '@pinia/testing'
import TaskForm from './TaskForm.vue'

describe('TaskForm.vue', () => {
  it('Renders a Task Form Component', () => {
    const wrapper = mount(TaskForm, { global: {
      plugins: [createTestingPinia()],
    }})
    expect(wrapper.text()).toMatch('Task Manager');
    const taskName = wrapper.find('#taskName');
    expect(taskName.exists()).toBe(true);
    const taskDescription = wrapper.find('#taskDescription');
    expect(taskDescription.exists()).toBe(true);
    const submitButton = wrapper.find('button.form__submit');
    expect(submitButton.exists()).toBe(true);

    wrapper.find<HTMLButtonElement>('button.form__submit').trigger('click');
    expect(wrapper.vm.taskForm.localError).toBe('Form validation failed')
  })

  test('button trigger event', async () => {
    const wrapper = mount(TaskForm, { global: {
      plugins: [createTestingPinia()],
    }})

    const button = wrapper.find<HTMLButtonElement>('button.form__submit');
    expect(button.attributes('type')).toBe('submit')
  })
})