import { setActivePinia, createPinia } from 'pinia'
import { useCounterStore } from '@/stores/counter'

describe('Counter store', () => {
  beforeEach(() => {
    setActivePinia(createPinia())
  })

	it('increments counter value', () => {
    const counterStore = useCounterStore()
    expect(counterStore.count).toBe(0)
    counterStore.increment()
    expect(counterStore.count).toBe(1)
  })

	it('decrements counter value', () => {
    const counterStore = useCounterStore()
    expect(counterStore.count).toBe(0)
    counterStore.decrement()
    expect(counterStore.count).toBe(-1)
  })
})